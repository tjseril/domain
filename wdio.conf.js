exports.config = {
    specs: [
        './Tests/DomainTest.js'
    ],
    exclude: [
        // 'path/to/excluded/files'
    ],
    maxInstances: 2,
    capabilities: [
      {
        maxInstances: 1,
        browserName: 'firefox'
      }
      ,
      // { 
      //   maxInstances: 1,
      //   browserName: 'chrome'
      // }
    ],
    sync: true,
    logLevel: 'silent',
    coloredLogs: true,
    //
    // Set specific log levels per logger
    // loggers:
    // - webdriver, webdriverio
    // - @wdio/applitools-service, @wdio/browserstack-service, @wdio/devtools-service, @wdio/sauce-service
    // - @wdio/mocha-framework, @wdio/jasmine-framework
    // - @wdio/local-runner, @wdio/lambda-runner
    // - @wdio/sumologic-reporter
    // - @wdio/cli, @wdio/config, @wdio/sync, @wdio/utils
    // Level of logging verbosity: trace | debug | info | warn | error | silent
    // logLevels: {
    //     webdriver: 'info',
    //     '@wdio/applitools-service': 'info'
    // },
    //
    // If you only want to run your tests until a specific amount of tests have failed use
    // bail (default is 0 - don't bail, run all tests).
    bail: 0,
    baseUrl: '/',
    waitforTimeout: 60000,
    connectionRetryTimeout: 120000,
    connectionRetryCount: 3,
    framework: 'mocha',
    services: ['selenium-standalone'],
    reporters: [
      'spec',
      // ['junit', {
      //   outputDir: './Output',
      //   outputFileFormat: function(options) { // optional
      //       return `results-${options.capabilities.browserName}.xml`
      //   }
      // }],
      ['mochawesome',{
        outputDir: './Output',
        outputFileFormat: function(opts) { 
          return `DomainTestResults-${opts.capabilities.browserName}-${Date.now().toString()}.json`
        }
      }]
    ],
    mochaOpts: {
        ui: 'bdd',
        timeout: 60000
    },
    before: function() {
      var chai = require('chai');
      var chaiAsPromised = require('chai-as-promised');
      chai.use(chaiAsPromised);
      chai.should();
      global.expect = chai.expect;
    },
    after: function(failures, pid) {
    },
    onComplete: function() {
    }
  }
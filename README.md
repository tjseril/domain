# Domain.com.au Test #

This repository contains a suite of tests for testing the following procedures in the Domain.com.au website using NodeJS and WebdriverIO:
1. Navigating to the Domain website and searching using the keyword 'str'
2. Selecting the first search result and verifying the listing displayed is the same as the first search result.

The tests use **mocha** to structure the tests and **chai** for assertions.

## Usage Instructions ##

This project requires NodeJS with npm (latest versions 14.3.0 and 6.14.5, respectively). To ensure that both are installed, run `node -v && npm -v` in the command line. To install npm, run `npm install` command from the working directory.

The `package.json` outlines the packages used. 

The tests can run both Chrome and Firefox in parallel - go to `wdio.conf.js` and ensure both browsers are enabled in `capabilities` (currently, Chrome is commented out so the tests only run in Firefox).

### To Run The Project ###

1. Open up a terminal window and navigate to your local working directory
2. Ensure that the `node_modules` folder has been created using `package.json` (this is usually created after npm installation)
3. To execute tests, run the comand `npx wdio`
4. Results are logged in the `Output` folder in JSON format using Mochawesome reporter

var DomainPageList = (function(){
    function DomainPageList(){
        this.baseUrl = 'http://www.domain.com.au';
        this.searchBox = '#fe-pa-domain-home-typeahead-input'; //'.1qjxpow'
        this.searchButton = '.css-1yqt5io';
        this.firstResult = '.is-first-in-list.css-1b4kfhp';
        this.addressFirstResult = '.css-1l7mgse';
        this.linkFirstResult = '.css-1abqvvq';
        this.addressListing = '.listing-details__listing-summary-title-address';
    }
    return DomainPageList;
})();

module.exports = DomainPageList;
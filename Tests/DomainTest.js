const chai = require('chai');
const expect = chai.expect;
const assert = chai.assert;

var DomainPageList = require('../PageObjects/DomainPageListObject.js');
var domainElements = new DomainPageList();

var DomainTestData = require('../Input/DomainTestData.js');
var testData = new DomainTestData();

describe('Domain Website Test', function(){
    it('Navigate to Domain landing page', function(){
        browser.url(domainElements.baseUrl);
        $(domainElements.searchBox).waitForExist();
        $(domainElements.searchButton).waitForExist();
        expect($(domainElements.searchBox).isEnabled()).to.be.true;
        // expect
        // $(domainElements.searchBox).setValue(testData.searchString);
        // $(domainElements.searchButton).click();
    });

    it('Search using input string', function(){
        $(domainElements.searchBox).setValue(testData.searchString);
        $(domainElements.searchButton).click();
        $(domainElements.firstResult).waitForExist();

        expect($(domainElements.addressFirstResult).isExisting()).to.be.true;
        expect($(domainElements.linkFirstResult).isExisting()).to.be.true;
    });

    it('View and verify first search result on property listing page', function(){
        var addressComparison = $(domainElements.addressFirstResult).getText();
        $(domainElements.linkFirstResult).click();
        $(domainElements.addressListing).waitForExist();
        assert.equal($(domainElements.addressListing).getText().toLowerCase(),addressComparison.toLowerCase());
    });
});
